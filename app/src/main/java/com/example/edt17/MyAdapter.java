package com.example.edt17;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Post> insta = new ArrayList<>();

    //private final RequestManager requestManager;


    public MyAdapter(Context context, ArrayList<Post> insta) {
        this.context = context;
        this.insta = insta;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.grid_data, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(insta.get(position).getUrlImg()).fit().centerCrop().into(holder.imgGrid);
        holder.textTitle.setText(insta.get(position).getTitle());
        holder.gridLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailFragment detailFragment = DetailFragment.newInstance(
                        insta.get(holder.getAdapterPosition()).getTitle(),
                        insta.get(holder.getAdapterPosition()).getAuthor(),
                        insta.get(holder.getAdapterPosition()).getDesc(),
                        //insta.get(holder.getAdapterPosition()).getUrlImg(),
                        insta.get(holder.getAdapterPosition()).getUrlImg2()
                );
                FragmentManager fragmentManager=((FragmentActivity)context).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction=fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.main_frame, detailFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

    }

    @Override
    public int getItemCount() {
        return insta.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgGrid;
        private TextView textTitle;

        ConstraintLayout gridLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imgGrid = itemView.findViewById(R.id.imgGrid);
            textTitle = itemView.findViewById(R.id.textTitle);
            gridLayout = itemView.findViewById(R.id.gridLaout);


        }
    }




}

package com.example.edt17;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {

    ArrayList<Post> insta  = new ArrayList<>();

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        RecyclerView rView = view.findViewById(R.id.rView);

        initData();

        MyAdapter myAdapter = new MyAdapter(view.getContext(), insta);
        rView.setAdapter(myAdapter);
        rView.setLayoutManager(new GridLayoutManager(view.getContext(), 3, RecyclerView.VERTICAL, false));


        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //implementation here
    }


    private void initData(){
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2021/08/09/abejas-de-la-miel_551cf738_2000x1333.jpg",
                "https://www.nationalgeographic.com.es/medio/2018/02/27/foto-abejas__1280x720.png",
                "Abeja",
                "Anthophila",
                "Las abejas son unos insectos extremadamente sociables que viven en colonias que se establecen en forma de enjambres y en los que se organizan en una estricta jerarquía de tres rangos sociales: la abeja reina, los zánganos y las abejas obreras. Habitan en todos los continentes de la Tierra excepto en la Antártida, y se trata de uno de los insectos más antiguos, del que se sabe, puebla nuestro planeta desde hace más de de 30 millones de años. Se conocen más de 20.000 subespecies distintas de abeja divididas en 7 familias reconocidas. \n" +
                        "\n" +
                        "Las abejas son los insectos polinizadores por excelencia y tienen una función esencial para el equilibrio de la naturaleza,  ya que contribuyen activamente a la supervivencia de muchas especies de plantas que se reproducen gracias al transporte de polen que llevan a cabo estos pequeños animales al alimentarse del néctar de las flores. Muchas de estas plantas las usamos los seres humanos para producir algunos de nuestros alimentos. Viven una media de cinco años y no miden más de 1,5 centímetros."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2021/03/23/aguila-calva-halieetus-leucocephalus_a540ab37_1674x1790.jpg",
                "https://www.nationalgeographic.com.es/medio/2018/02/27/aguila__1280x720_2.jpg",
                "Águilas",
                "Majestuosas, sigilosas, veloces, inteligentes y hábiles: si el mar es de los tiburones y la sabana de los leones, la hegemonía del reino de los cielos es delas águilas. Pertenecientes a las familia Accipitridae, este tipo de aves sobrevuelan los cielos de todo el mundo excepto los antárticos. Depredadores por excelencia estos pájaros son temibles aves de presa.  Cuentan con un pico fuerte y robusto acabado en punta y hacia abajo que les facilita la tarea de separar los pedazos de carne de sus víctimas. También gozan de un sentido de la vista extraordinario con el que pueden divisar a sus presas a grandes distancias y además, sus potentes y robustas garras les permiten atrapar animales más grandes que ellas y trasladarlos por el aire. \n" +
                        "\n" +
                        "Pese a ser símbolo de numerosas naciones por sus encomiables atributos y su majestuoso porte, y pese a situarse en lo más alto de las cadenas alimentarias de las que forma parte, numerosas especies de águilas se encuentran en la actualidad en peligro de extinción debido a la progresiva pérdida de hábitats, la ausencia de presas e incluso en algunos lugares la actividad de los cazadores.  Un ejemplo de ello es el águila imperial blanca, una de las especies en peligro de extinción en España."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2018/11/23/anemona-fluorescente_0d132e47_1500x1001.jpg",
                "https://www.nationalgeographic.com.es/medio/2018/02/27/anemona__1280x720.jpg",
                "Anémonas",
                "Actiniaria",
                "Las anémonas marinas o actiniarias son un tipo de animal invertebrado que permanece la mayor parte de su vida anclado al sustrato y que tiene se alimenta de una dieta carnívora. Puesto que no puede moverse, la única manera de cazar algún animal es esperando a que pase lo suficientemente cerca como para alcanzarlo con sus tentáculos. \n" +
                        "\n" +
                        "Existen más de 1.000 especies diferentes descritas y pueden llegar a medir casi dos metros de alto. Están adheridas al suelo marino mediante un pie, su cuerpo tiene forma cilíndrica y en el centro se halla la boca, a donde se hace llegar la comida mediante los tentáculos. Estos se ponen en activo cuando notan cualquier pequeño roce y se lanzan sobre la presa y le inoculan un líquido paralizante.\n" +
                        "\n" +
                        "Suelen vivir cerca de la costa en todo tipo de aguas, con temperaturas templadas o frías. Son un tipo de animal que establece simbiosis con otros como por ejemplo algunas algas, a las que protege de la exposición a la luz solar, o el pez payaso, que tiene una fina capa de mucosa alrededor de su cuerpo que lo protege del veneno de las anémonas."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2018/02/27/rana__1280x720.jpg",
                "https://www.nationalgeographic.com.es/medio/2021/11/11/rana-de-boca-estrecha-de-matang-microhyla-borneensis_e73c9bf2_1254x836.jpg",
                "anfibios",
                "Amphibia",
                "Su nombre proviene del griego y significa \"ambos medios\", pues su vida transcurre entre el medio acuático y el terrestre. Se tratan los ancestros de los anfibios del primer grupo de vertebrados que colonizó el continente y se adaptó a una vida semiterrestre. Se encuentran prácticamente en todas las regiones del mundo excepto en aquellas donde las condiciones climáticas son más duras como el Ártico, la Antártida y los desiertos más extremos.  Los anfibios se caracterizan a diferencia del resto de vertebrados de pasar por diversos cambios y estadios morfológicos a lo largo de sus ciclos de vida. Transformaciones que de producirse de forma brusca reciben el nombre de metamorfosis.\n" +
                        "\n" +
                        "Un anfibio típico tiene una etapa larval que se gesta sin excepción en un medio acuático y durante la cual respira a través de branquias. En su etapa adulta, aunque no dejan de depender del agua, suelen estar menos atada a esta, de modo que muchas especies suelen desarrollar pulmones cuya función se ve reforzada por la respiración a través de la piel que muchos de estos animales son capaces de realizar.  No obstante existen también algunas especies que conservan sus branquias  a lo largo de toda su vida. Otras también son son capaces de segregar sustancias tóxicas que usan para defenderse de los posibles depredadores.  Se conocen cerca de 7.500 especies diferentes divididos en tres principales grupos: el de los anuros, que incluye a saposy ranas;  el de los caudados que engloba a salamandras y ajolotes el de los gimnofiones, en el que podemos encontrar a las cecilias. Algunas de ellas se encuentran en peligro de desaparecer. Una de las especies más emblemáticas y curiosas es el ajolote (Ambystoma mexicanum), endémico del sistema lacustre del valle de México y en peligro crítico de extinción en el país por la contaminación del hábitat en el que vive."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2019/03/04/birupes-simoroxigorum_c975e782_699x393.jpg",
                "https://www.nationalgeographic.com.es/medio/2018/02/27/arana__1280x720.jpg",
                "Araña",
                "Araneae",
                "Las arañas son un conjunto de animales artrópodos muy abundante en todo el mundo y del que se conocen aproximadamente unas 45.000 especies diferentes. Son el orden más numeroso de la clase Arachnida y están lejanamente emparentadas con otros grupos de artrópodos, como los insectos. Se trata además de uno de los grupos más diversos, colocándose en cuanto al resto de organismos en el séptimo lugar respecto a su diversidad. \n" +
                        "\n" +
                        "Las arañas tienen el cuerpo dividido en dos partes denominadas tagmas, y cuentan con cuatro pares de patas. Se sabe que durante la prehistoria existieron algunas arañas que podían llegar a medir 50 centímetros, pero actualmente la más grande que existe tiene un tamaño de 30 centímetros. Por regla general son animales solitarios y depredadores de pequeños insectos a los cuales pueden dar caza a través de técnicas muy variadas. Algunas, de hecho, poseen potentes venenos los cuales un pequeña cantidad, puede acabar con la vida de un ser humano.\n" +
                        "\n" +
                        "Son capaces de producir seda que usan para tejer sofisticadas telarañas principalmente con el objetivo de cazar, aunque tienen múltiples utilidades. A pesar de que algunas de ellas tienen cuatro pares de ojos, la mayoría no gozan de un gran sentido de la vista. Viven aproximadamente durante un año y se encuentran en prácticamente todas las partes del mundo exceptuando la Antártida."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2020/12/01/drey-dreaming_f95a42ff_1280x960.jpg",
                "https://www.nationalgeographic.com.es/medio/2021/06/29/sciurus-vulgaris_ec9e1ba8_2000x1283.jpg",
                "Ardilla",
                "Sciuridae",
                "13 centímetros mide la ardilla más pequeña y casi un metro la más grande del mundo, contando que la responsabilidad de gran parte de este tamaño es de la cola. Este mamífero roedor puede encontrarse por todo el mundo excepto en Oceanía y la Antártida. Escaladores experimentados, tan solo bajan al suelo para buscar comida como frutos secos, semillas, raíces y hojas, y en alguna ocasión huevos o pájaros muy pequeños. Sus dientes no dejan de crecer en toda su vida, por lo cual deben limarlos constantemente. \n" +
                        "\n" +
                        "Son animales diurnos, con excepción de las ardillas voladoras, y son muy sensibles a los cambios de temperatura así como a los climas extremos, algunas hibernan con el frío o permanecen prácticamente inactivas con el calor. Tienen muy desarrollados el sentido del olfato, del oído y de la vista y viven entre 5 y 10 años en libertad. "
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2018/02/27/ballenas-en-peninsula-valdes__1280x720.jpg",
                "https://www.nationalgeographic.com.es/medio/2021/11/01/una-ballena-jorobada-se-alimenta-de-la-costa-de-california_5c5ce6ef_2000x1500.jpg",
                "Ballenas",
                "Balaenidae",
                "Dentro de el orden de los cetáceos y concretamente del de los cetaceos misticetos se encuentran las ballenas, las cuales conforman la familia de los balaénidos o de las ballenas barbadas. Estas están clasificados en tan solo cuatro especies divididas en dos géneros, Balaena y Eubalaena. Estos animales pueden llegar a medir entre 25 y 32 metros y los ejemplares más grandes pueden llegar a pesar hasta 180 toneladas. De hecho, una de estas especies de balaenidos, la ballena azul, es el animal más grande del mundo en la actualidad. \n" +
                        "\n" +
                        "Estos mamíferos, a diferencia de los peces, tienen la cola dispuesta en horizontal, lo que les facilita la ascensión a la superficie, donde deben subir para respirar. Pueden permanecer bajo el agua aproximadamente una hora. Con grandes requerimientos energéticos, las ballenas se alimentan generalmente de pequeños crustáceos y de krill, que obtienen por la filtración del agua del mar a través de sus barbas.  Habitan en todos los océanos en los que se desplazan en largos viajes migraciones desde los mares fríos, donde se alimentan, a los más cálidos, donde se aparean y reproducen. Después de una gestación de casi doce meses, dan a luz a una sola cría,  cuya esperanza de vida está en torno a los 30 años. "
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2018/09/14/wildwonders54__1400x857.jpg",
                "https://www.nationalgeographic.com.es/medio/2018/04/10/ritos-funerarios_f889f812.jpg",
                "Buitre",
                "Falconiformes",
                "Los buitres son aves rapaces del orden Falconiformes.  Una cabeza exenta de plumas proporciona a los buitres su imagen más característica, algo que facilita las labores de higiene después de una comida.\n" +
                        "\n" +
                        "Estas aves pueden encontrarse en cualquier parte del mundo con excepción de la Antártida y Oceanía, los únicos lugares del mundo donde no les veremos acechar a sus posibles víctimas.  Salvo contadas excepciones, los buitres casi nunca matan directamente a los animales de los que se alimentan, sino que son aves carroñeras y a pesar de su injusta reputación,  cumplen con una importante función ecológica retirando materia orgánica en descomposición y reincorporándola directamente a las redes tróficas animales. \n" +
                        "\n" +
                        "Todas las especies de buitre están adaptados para volar a gran altura. Debido a su gran tamaño, los buitres deben aprovechar las corrientes calientes de aire para elevarse en las extensas llanuras sobre las que planean para identificar con una privilegiada vista la carroña que les sirve de alimento. Pueden volar durante largo rato, en círculo, sin apenas mover las alas aprovechando al máximo la energía necesaria para el vuelo. A diferencia de los buitres del Viejo Mundo, los buitres americanos poseen además un gran olfato."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2021/10/18/caballada-en-las-estepas-de-mongolia-interior_078d9e7a_2000x1445.jpg",
                "https://www.nationalgeographic.com.es/medio/2021/10/18/caballos-trotan-en-las-estepas-de-mongolia-interior_bc0bcb5d_2000x1333.jpg",
                "Caballo",
                "Equus caballus",
                "Los caballos y los humanos tienen una relación muy antigua. Se cree que los nómadas asiáticos fueron los que probablemente domesticaron a los primeros caballos hace unos 4.000 años, momento desde el cual estos animales siguieron siendo esenciales para muchas sociedades hasta el advenimiento del motor. De hecho los  caballos todavía tienen reservado un lugar de honor en muchas culturas, y a menudo se encuentran vinculados a multitud de hazañas bélicas.\n" +
                        "\n" +
                        "Los caballos son animales mamíferos perisodáctilos –en cuyas extremidades poseen dedos terminados en pezuñas– que pertenecen a la familia de los équidos. Son herbívoros y el periodo de gestación de las hembras es de unos 11 meses, después del cual nace tan solo una cría. Existe únicamente una especie de caballo doméstico, sin embargo podremos encontrar alrededor de 400 razas diferentes que se especializan en todo tipo de tareas, desde fuertes y resistentes animales usados para tirar de los aperos del campo hasta los más veloces empleados en las carreras. \n" +
                        "\n" +
                        "Los caballos salvajes por lo general se reúnen en grupos de 3 a 20 animales. Un semental lidera el grupo, conformado por varias yeguas y ejemplares jóvenes. Cuando los machos jóvenes se convierten en potros, alrededor de los dos años de edad, el semental los expulsa. Desde entonces estos vagan con otros machos jóvenes hasta que pueden hacerse con su propio harén.  Los caballos viven alrededor de 25 años y el color de su pelaje o capa puede ser muy variable. Se desplazan de tres formas diferentes: al paso, al trote y al galope. Los caballos duermen de forma fraccionada y son capaces de hacerlo de pie, aunque para descansar profundamente siempre lo harán sentados en el suelo."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2021/02/09/milk-feeding_d88cdb6c_1333x2000.jpg",
                "https://www.nationalgeographic.com.es/medio/2012/09/26/wildwonders08.jpg",
                "Cachalote",
                "Physeter macrocephalus",
                "Moby Dick es el cachalote más conocido en el mundo entero, pero con su particular aspecto estos enormes mamíferos son fáciles de reconocer: una cabeza gigante que termina en una frente con un ángulo de casi 90 grados y que además alberga el cerebro más grande que ninguna otra criatura haya tenido en la Tierra. \n" +
                        "\n" +
                        "Desde su larga cola hasta el hocico pueden llegar a medir 20 metros y pesar hasta unas 40 toneladas aproximadamente. Se trata del depredador vivo más grande que habita en nuestro planeta.  Se alimenta principalmente de calamares gigantes y pulpos, y puede llegar a ingerir casi una tonelada de alimento en tan solo un día. Viven en los océanos de todo el mundo, incluyendo las zonas árticas. \n" +
                        "\n" +
                        "Esta especie animal emite un tipo de sonidos muy característicos llamados chasquidos, cuya función no ha sido descubierta aunque se especula con la posibilidad de que los usen como medida de orientación. \n" +
                        "\n" +
                        "Durante el siglo XVII y hasta finales del XX se cazaba cachalotes con el fin de usar su carne y alguno de sus fluidos para productos cosméticos. Debido a su enorme tamaño, estos animales eran capaces de defenderse contra los primeros ataques de los balleneros. Uno de los casos más conocidos es el del barco ballenero  Essex que fue hundido por un cachalote. En cuanto a su estado de conservación se encuentra clasificado en la actualidad por la Union Internacional de Conservación de la Naturaleza como una especie vulnerable."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2021/11/29/camaleon-pantera-furcifer-pardalis_2fef4289_2000x1334.jpg",
                "https://www.nationalgeographic.com.es/medio/2019/01/03/camaleon-de-parson_6a22e34f_1500x1000.jpg",
                "Camaleon",
                "Chamaeleonidae",
                "Los camaleones son una familia de pequeños saurópsidos escamosos -reptiles- de la cual se conocen cerca de 161 especies diferentes. Existe una amplia variedad de camaleones en cuanto a su tamaño: desde los más pequeños de la familia Brookesia,  los cuales apenas alcanzan los 2,9 centímetros de largo, hasta los 80 centímetros que puede alcanzar un ejemplar de Calumna parsonii. \n" +
                        "\n" +
                        "Y si hay un animal al que relacionamos directamente con la capacidad de pasar desapercibido en el medio este es el camaleón. Esta pequeña especie de lagarto tiene la habilidad de cambiar de color para camuflarse con el medio, algo que también delata su estado de ánimo, y mecanismo por el cual se comunica con los de su misma especie. \n" +
                        "\n" +
                        "También son muy reconocibles por su larga lengua que utilizan para alcanzar a las presas, llevárselas a la boca y engullirlas enteras –en ocasiones la lengua puede llegar a ser más larga que su propio cuerpo.\n" +
                        "\n" +
                        "Otro de sus rasgos más llamativos son los ojos, que pueden mover de forma independiente el uno del otro, lo cual les permite tener un campo visual de casi 360 grados.\n" +
                        "\n" +
                        "Se reproduce por huevos y se alimenta de insectos y pequeños animales, así como de plantas y algunas frutas. La mayoría de estos ejemplares habitan en África y Madagascar, aunque también pueden encontrarse en Europa y algunas partes de Asia. "
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2011/08/31/tuareg07_670x457.jpg",
                "https://www.nationalgeographic.com.es/medio/2015/08/06/ori-x-11-1s_1000x650.jpg",
                "Camello",
                "Camelus",
                "Con sus dos características jorobas, habitan en el norte de África, en parte de Oriente Medio y Asia central. Las jorobas o gibas son una característica propia de este tipo de mamíferos de las zonas áridas que usan para almacenar un tipo de grasa de la que, después de ser metabolizada, pueden extraer agua.\n" +
                        "\n" +
                        "Además cuentan con otra serie de adaptaciones a los duros climas en los que habitan. Estos animales rara vez sudan, lo que les ayuda a conservar el agua en el interior de su cuerpo durante largos períodos de tiempo. Además, en invierno, las plantas de las que se alimentan pueden albergar el agua suficiente para mantener a un camello con vida durante varias semanas. Sin embargo, pese a poder resistir a las condiciones más duras, un camello puede comportarse como una autentica esponja, siendo capaz de beber, cuando tiene la oportunidad, hasta 115 litros de agua de una sola vez. \n" +
                        "\n" +
                        "Suelen vivir alrededor de 45 años y miden aproximadamente 1,85 metros. En las zonas donde han sido domesticados se usan como animales de carga en las largas travesías por los desiertos, y son capaces de correr a casi 70km/h. Sus grandes ojos están protegidos por pestañas largas debido al fuerte viento que suele azotar las llanuras desérticas. "
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2020/10/04/young-look_c8abb40f_1254x837.jpg",
                "https://www.nationalgeographic.com.es/medio/2018/02/27/cebras__1280x720.jpg",
                "Cebra",
                "Equidae",
                "Ningún animal en el mundo tiene un pelaje tan distintivo como el de la cebra. Las rayas de cada ejemplar son tan singulares como para los seres humanos lo son las huellas dactilares.  No existen dos exactamente iguales y además, el pelaje de cada una de las tres especies distintas descritas de cebra obedece a un patrón cromático distinto. \n" +
                        "\n" +
                        "¿Por qué las cebras tienen rayas ? Los científicos no están seguros, pero muchas teorías se centran en su utilidad como  forma de camuflaje. Los patrones pueden dificultar que los depredadores las identifiquen y distorsionar la apariencia de la distancia a la que se encuentran, sobre todo al amanecer y al anochecer.  Debido a su singularidad, las rayas también pueden ayudar a las cebras a reconocerse.\n" +
                        "\n" +
                        "Se trata de animales sociales que pasan el tiempo en manada.  Suelen organizarse en grupos familiares que en algunas ocasiones que unen con otros grupos, pero la familia original siempre permanece unida. Sus principales depredadores son los leones y las hienas, unas amenazas contra las que luchan y de las que se protegen también colaborando en grupo. Son animales exclusivamente herbívoros, de la familia équida –como el caballo o el asno– y se conocen tres subespecies distintas.  "
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2021/02/09/milk-feeding_d88cdb6c_1333x2000.jpg",
                "https://www.nationalgeographic.com.es/medio/2021/02/02/tonga_708300c2_2000x1326.jpg",
                "Cetaceo",
                "Cetacea",
                "Ballenas, cachalotes, delfines, orcas, marsopas y casi 80 especies diferentes más pertenecen a este orden de animales mamíferos que habitan en el medio acuático. El ejemplar más grande, la ballena azul, puede alcanzar los 30 metros de largo y el más pequeño, una marsopa, mide aproximadamente 140 centímetros. \n" +
                        "\n" +
                        "En la espalda cuentan con una aleta dorsal que les aporta estabilidad y su cola termina en una aleta caudal horizontal que aporta velocidad a sus desplazamientos. Son animales de respiración pulmonar, por lo que deben subir de manera regular a la superficie. El periodo de gestación de las hembras varía entre 7 y 13 meses y viven aproximadamente entre 20 y 30 años, con algunas excepciones."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2021/10/27/macho-alfa-de-chimpance-caminando-sobre-sus-4-extremidades_afb22908_2000x1333.jpg",
                "https://www.nationalgeographic.com.es/medio/2021/10/27/dos-chimpances-adultos_1c6ea50f_2000x1333.jpg",
                "Chimpance",
                "Pan troglodytes",
                "El hábitat natural de este primate se encuentra en las junglas, los bosques y las sabanas de África tropical, donde viven organizados en comunidades que pueden estar formadas por más de 100 miembros.\n" +
                        "\n" +
                        "Viven aproximadamente unos 45 años, algunos pesan más de 50 kilos y miden entre 1,20 y 1,70 metros.  A pesar de que normalmente caminan a cuatro patas apoyándose sobre sus nudillos, los chimpancés también pueden permanecer estáticos y caminar en una posición vertical. Del mismo modo, estos primates se desenvuelven con tremenda eficacia sobre las copas de los árboles en los árboles, de donde obtienen la mayor parte de los alimentos de su dieta. Se alimentan principalmente de hierbas y plantas, pero ocasionalmente comen insectos y presas pequeñas.\n" +
                        "\n" +
                        "El chimpancé es el ser vivo más próximo al ser humano, se calcula que nuestros linajes se separaron hace alrededor de 7 millones de años y actualmente compartimos casi un 98% de nuestro ADN con ellos. Se trata de una de las pocas especies animales que, al igual que el Homo sapiens, emplea herramientas: usan palos para capturar insectos en sus nidos o extraer las larvas de troncos de algunos árboles. También usan piedras para romper las cáscaras de algunos frutos secos  y utilizar las hojas para recolectar agua potable. A los chimpancés se les puede enseñar a usar algún lenguaje de signos humano básico.\n" +
                        "\n" +
                        "Las hembras pueden dar a luz en cualquier época del año, por lo general a una sola cría que se aferra a su madre y más tarde monta en su espalda hasta la edad de dos años. Las hembras alcanzan la edad reproductiva a los 13 años, mientras que los machos no son considerados adultos hasta que tienen 16 años.\n" +
                        "\n" +
                        "Aunque los chimpancés y los seres humanos están estrechamente relacionados, estos simios han sufrido mucho a manos de los segundos encontrándose en la actualidad en la lista de animales en peligro de extinción. "
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2018/02/27/cocodrilo__1280x720.jpg",
                "https://www.nationalgeographic.com.es/medio/2021/06/05/fish-caught-by-surprise_0db5d6b7_2000x1429.jpg",
                "Cocodrilo",
                "Crocodylidae",
                "Se denomina comúnmente cocodrilo a cualquiera de las 14 especies pertenecientes a la familia de los saurópsidos arcosaurios Crocodylidae. Estos reptiles viven en África, América, Asia y Australia y los científicos estiman que aparecieron hace unos 55 millones de años, durante el Eoceno. Con un modo de vida semiacuático, y con una tendencia a vivir en congregaciones, los cocodrilos son depredadores y se alimentan principalmente de otros animales vertebrados, aunque no es raro encontrar algunas especies que se alimenten de moluscos y crustáceos.\n" +
                        "\n" +
                        "Los cocodrilos están dotados de una piel escamosa, muy dura y seca. Sus fosas nasales y sus ojos se encuentran en la parte superior de la cabeza, lo que le permite ver y respirar mientras permanece en el agua. Suelen pasar la mayor parte del día parados, a la espera de que una presa se acerque lo suficiente como para lanzar un ataque súbito. Tienen además cuerpos pesados y metabolismos generalmente lentos, aunque pueden controlar la velocidad de su digestión según la abundancia de presas y de la temperatura ambiente. \n" +
                        "\n" +
                        "Los cocodrilos del Nilo, por ejemplo, pueden llegar a medir 6 metros y pesar hasta 730 kilos. Sin embargo, su tamaño medio es de unos 5 metros y unos 250 kilos. Se pueden encontrar en el África subsahariana, la cuenca del Nilo y en Madagascar y pueden llegar a vivir entre 50 y 80 años, dependiendo de la especie. "
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2021/11/14/coral-petreo_3fe8c6bf_1273x847.jpg",
                "https://www.nationalgeographic.com.es/medio/2021/10/12/isla-fenjiezhou-china_8a7c8ebf_2000x1108.jpg",
                "Coral",
                "Anthozoa",
                "Los corales marinos son animales coloniales que pueden alcanzar tamaños enormes, formando zonas muy extensas tropicales o subtropicales ocupadas por este ser vivo como la Gran Barrera de Coral australiana o el arrecife mesoamericano en el Mar Caribe. Algunos se alimentan del plancton que pueden atrapar con sus tentáculos y otros se nutren de las algas que viven entre sus recovecos y con las que conviven en perfecta simbiosis. \n" +
                        "\n" +
                        "Algunos corales se reproducen de forma asexual por gemación o división, pero la mayoría lo hacen de forma sexual: soltando las células sexuales al mar. Muchos de estos huevos serán devorados por peces y otros animales, pero la cantidad es tan grande que termina siendo una reproducción exitosa. Dado que estos desoves se producen todos a la vez siguiendo ciertas señales naturales como la puesta de sol o un cambio de temperatura, resulta una imagen espectacular."
        ));
        insta.add(new Post(
                "https://www.nationalgeographic.com.es/medio/2019/01/16/atlantic-spotted-dolphins_46007099_650x488.jpg",
                "https://www.nationalgeographic.com.es/medio/2019/02/27/the-heat-run_491a95eb_1000x667.jpg",
                "Delfin",
                "Delphinidae",
                "Los delfines son un tipo de mamíferos cetáceos pertenecientes a la familia Delphinidae. Pueden vivir más de 30 años en cautividad y tienen una longitud de 3,5 metros aproximadamente. En la naturaleza, estos nadadores elegantes pueden alcanzar velocidades de más de 30 kilómetros por hora. Durante sus desplazamientos surgen a menudo a la superficie del mar para respirar, haciéndolo una media de dos o tres veces por minuto. Viajan en grupos sociales y se comunican entre sí por un complejo sistema de chirridos y silbidos que del mismo modo les sirve para ecolocalizar a sus presas. Pueden producir hasta 1.000 \"ruidos de clic\" por segundo. Estos sonidos viajan bajo el agua hasta que encuentran objetos, luego regresan a sus remitentes de delfines, revelando la ubicación, tamaño y forma de su objetivo. Se trata de animales muy inteligentes que no dejan de sorprender a los científicos pues han mostrado capacidades de aprendizaje y cognición muy superiores a las de otras especies. \n" +
                        "\n" +
                        "Se encuentran en los océanos tropicales y otras aguas cálidas de todo el mundo. Antaño fueron cazados extensamente por su carne y grasas usadas en lámparas de aceite y para cocinar. Hoy en día, la pesca del delfín se encuentra muy limitada,  no obstante los delfines siguen amenazados por la pesca comercial de otras especies como el atún, y pueden quedar mortalmente enredados en redes y otros equipos de pesca."
        ));
    }


}
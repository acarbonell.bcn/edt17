package com.example.edt17;

public class Post {
    private String urlImg;
    private String urlImg2;
    private String title;
    private String author;
    private String desc;

    public Post(String urlImg, String urlImg2, String title, String author, String desc) {
        this.urlImg = urlImg;
        this.urlImg2 = urlImg2;
        this.title = title;
        this.author = author;
        this.desc = desc;
    }

    public Post(String urlImg, String urlImg2, String title, String desc) {
        this.urlImg = urlImg;
        this.urlImg2 = urlImg2;
        this.title = title;
        this.desc = desc;
    }

    public String getUrlImg() {
        return urlImg;
    }

    public void setUrlImg(String urlImg) {
        this.urlImg = urlImg;
    }

    public String getUrlImg2() {
        return urlImg2;
    }

    public void setUrlImg2(String urlImg2) {
        this.urlImg2 = urlImg2;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
